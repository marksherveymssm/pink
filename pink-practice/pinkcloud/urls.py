from django.conf.urls import patterns, include, url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.static import serve
from django.conf import settings

from views import IndexView, PrivacyView
from beta.views import VendorView

from django.contrib import admin
from django.views.generic.simple import redirect_to
from profiles.views import *
from profiles.forms import *
from ajax_select import urls as ajax_select_urls

coordinator_wizard = [
    UserForm, CoordinatorForm,
    QuizPart1Form, QuizPart2Form,
    QuizPart3Form, QuizPart4Form,
    QuizPart5Form
]

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view()),
    url(r'^privacy/$', PrivacyView.as_view()),
    url(r'^login/$', anonymous_required(auth_views.login), {'template_name': 'registration/login.html'},
        name='auth_login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'},
        name='auth_logout'),
    url(r'^invite/vendor/$', anonymous_required(VendorView.as_view())),
    url(r'^profile/', include('pinkcloud.profiles.urls')),
    #url(r'^registration/vendor/$', VendorWizard.as_view([UserForm, VendorForm], template_name="registration/vendor.html")),
    url(r'^registration/coordinator/$', anonymous_required(CoordinatorWizard.as_view(coordinator_wizard, template_name="registration/coordinator.html"))),
    url(r'^registration/coordinator/thanks/$', anonymous_required(CoordinatorThanksView.as_view())),
    url(r'^registration/register/', redirect_to, {'url':'/'}),
    url(r'^registration/', include('registration.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^mail/', include('postman.urls')),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),

    url(r'^static/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
    url(r'^media/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^media/resumes/(?P<path>.*)$', staff_member_required(serve), { 'document_root': settings.MEDIA_ROOT + "/resumes/" }), 
)
