"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'pc9.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'pc9.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from postman.models import Message
from registration.models import RegistrationProfile

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for pinkcloud.
    """
    def init_with_context(self, context):
        self.children.append(modules.Group(
          title="Pinkcloud 9 Administrator Console",
          display="tabs",
          children = [
            modules.AppList(
              title="Invite Requests",
              models =('pinkcloud.beta.*',),
            ),
            modules.AppList(
                title=('Users and Profiles'),
                models =('pinkcloud.profiles.*','django.contrib.auth.*','registration.*',),
            ),
            modules.AppList(
              title="System Messages",
              models=('postman.*',)
            )
        ]))
        user = context["request"].user
        reg_count = RegistrationProfile.objects.filter(_status="untreated").count()
        self.children.append(modules.LinkList(
          title="Action Items",
          layout="stacked",
          children = (
            ['{0} Unread Messages'.format(Message.objects.inbox_unread_count(user)), '/mail/inbox/'],
            ['{0} Unapproved Registrations'.format(reg_count), '/admin/registration/registrationprofile/?_status__exact=untreated'],
           )
        ))

class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for pc9.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
