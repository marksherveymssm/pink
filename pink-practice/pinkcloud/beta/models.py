from django.db import models

class InviteRequest(models.Model):
  INVITE_TYPE = (
      (1, 'Bride/Groom'),
      (2, 'Vendor'),
      (3, 'Venue'),
  )
  email_address = models.EmailField( null=False, blank=False, unique=True)
  created = models.DateField(auto_now_add=True, null=False, blank=False)
  inviteType = models.PositiveSmallIntegerField(null=False, blank=False, choices=INVITE_TYPE)
