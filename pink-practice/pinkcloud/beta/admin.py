from django.contrib import admin
from models import InviteRequest

class InviteAdmin(admin.ModelAdmin):
  list_display = ('email_address', 'created', 'inviteType', )

admin.site.register(InviteRequest, InviteAdmin)
