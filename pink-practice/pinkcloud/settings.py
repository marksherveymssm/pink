from registration.settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
EMAIL_HOST_USER = "no-reply@pinkcloud9.com"
DEFAULT_FROM_EMAIL = "no-reply@pinkcloud9.com"
#EMAIL_USE_TLS = True
EMAIL_HOST = 'localhost'
#EMAIL_HOST_USER = 'no-reply@pinkcloud9.com'
#EMAIL_HOST_PASSWORD = 'p1nkcl0ud9'
EMAIL_PORT = 1025

ADMINS = (
     ('Gregory', 'tech@pinkcloud9.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'pinkcloudnine',                      # Or path to database file if using sqlite3.
        'USER': 'pinkcloudnine',                      # Not used with sqlite3.
        'PASSWORD': 'LfuPG5xg9TfXnmv',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

TIME_ZONE = 'America/Los_Angeles'

LANGUAGE_CODE = 'en-us'
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = '/var/www/vhosts/pinkcloud9.com/pink-practice/public'
MEDIA_URL = 'http://pinkcloud9.com/media/'
STATIC_ROOT = '/var/www/vhosts/pinkcloud9.com/pink-practice/public'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = 'http://pinkcloud9.com/static/'

#REGISTRATION
REGISTRATION_ALERT_EMAIL = 'jobs@pinkcloud9.com'
ACCOUNT_ACTIVATION_DAYS = 30
REGISTRATION_ACTIVATION_EMAIL = None
REGISTRATION_NOTIFICATION_RECIPIENTS = ( 'jobs@pinkcloud9.com', )
AUTH_PROFILE_MODULE = 'profiles.Profile'
LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"
#POSTMAN
POSTMAN_DISALLOW_ANONYMOUS = True
POSTMAN_AUTO_MODERATE_AS = True


STATICFILES_DIRS = (
    '/srv/pink/static/',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = 'l*w&amp;vt$2(b6+cneg4e!*&amp;2)(#)$k7+nf0i#)$b--(#o@7k0=9a'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'djangosecure.middleware.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'postman.context_processors.inbox',
)

ROOT_URLCONF = 'pinkcloud.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'pinkcloud.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/root/pink-practice/templates/'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    #'registration',
    #'registration.contrib.notification',
    'profiles',
    #'registration_defaults',
    'taggit',
    'taggit_autosuggest',
    'pagination',
    'postman',
    #'reviews',
    'reversion',
    #'djangosecure',
    #'django_bcrypt',
    'ajax_select',
    'uni_form',
    #'json_field',
    #'template_debug',
    'pinkcloud.beta',
    'pinkcloud.profiles',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

AJAX_LOOKUP_CHANNELS = {
    'user'  : ('pinkcloud.profiles.lookups', 'UserLookup')
}
AJAX_SELECT_BOOTSTRAP = True
AJAX_SELECT_INLINES = 'inline'

ADMIN_TOOLS_INDEX_DASHBOARD = 'pinkcloud.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'pinkcloud.dashboard.CustomAppIndexDashboard'

SECURE_FRAME_DENY = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True

