from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import TemplateView
from django.shortcuts import redirect

class IndexView(TemplateView):
  template_name = "index.html"

  def dispatch(self, request, *args, **kwargs):
    if request.user.is_anonymous():
      return super(IndexView, self).dispatch(request, *args, **kwargs)
    elif request.user.is_staff:
      return redirect("/admin/")
    else:
      return redirect("/profile/")

class PrivacyView(TemplateView):
  template_name = "privacy.html"
