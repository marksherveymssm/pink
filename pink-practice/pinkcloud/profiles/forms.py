from models import *
from django import forms
from registration.forms import RegistrationFormUniqueEmail
from uni_form.helper import FormHelper

class USPhoneNumberMultiWidget(forms.MultiWidget):
    """
    A Widget that splits US Phone number input into three <input type='text'> boxes.
    """
    def __init__(self,attrs=None):
        widgets = (
            forms.TextInput(attrs={'size':'3','maxlength':'3', 'class':'phone'}),
            forms.TextInput(attrs={'size':'3','maxlength':'3', 'class':'phone'}),
            forms.TextInput(attrs={'size':'4','maxlength':'4', 'class':'phone'}),
        )
        super(USPhoneNumberMultiWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split('-')
        return (None,None,None)

    def value_from_datadict(self, data, files, name):
        value = [u'',u'',u'']
        # look for keys like name_1, get the index from the end
        # and make a new list for the string replacement values
        for d in filter(lambda x: x.startswith(name), data):
            index = int(d[len(name)+1:]) 
            value[index] = data[d]
        if value[0] == value[1] == value[2] == u'':
            return None
        return u'%s-%s-%s' % tuple(value)

class ThreeWordsMultiWidget(forms.MultiWidget):
    """
    A Widget that splits US Phone number input into three <input type='text'> boxes.
    """
    def __init__(self,attrs=None):
        widgets = (
            forms.TextInput(attrs={'size':'25','maxlength':'25' }),
            forms.TextInput(attrs={'size':'25','maxlength':'25'}),
            forms.TextInput(attrs={'size':'25','maxlength':'25'}),
        )
        super(ThreeWordsMultiWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split(', ')
        return (None,None,None)

    def value_from_datadict(self, data, files, name):
        value = [u'',u'',u'']
        # look for keys like name_1, get the index from the end
        # and make a new list for the string replacement values
        for d in filter(lambda x: x.startswith(name), data):
            index = int(d[len(name)+1:]) 
            value[index] = data[d]
        if value[0] == value[1] == value[2] == u'':
            return None
        return u'%s, %s, %s' % tuple(value)


class UserForm(RegistrationFormUniqueEmail):
  first_name = forms.CharField(max_length=50,label="First Name")
  last_name = forms.CharField(max_length=50,label="Last Name")
  def __init__(self, *args, **kwargs):
    super(UserForm, self).__init__(*args, **kwargs)
    self.fields.keyOrder = [
        'first_name',
        'last_name',
        'username',
        'email1',
        'email2'
    ]

class VendorForm(forms.ModelForm):
  locations_served = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices = AREAS)
  class Meta:
    model = Vendor
    exclude = ('user','approved', 'tags','staff_pick','twitter','pinterest','youtube','vimeo')
  def __init__(self, *args, **kwargs):
    super(VendorForm, self).__init__(*args, **kwargs)
    self.fields['phone'].widget = USPhoneNumberMultiWidget()
    self.fields.insert(0, 'category', self.fields['category'])

class CoordinatorForm(forms.ModelForm):
  def __init__(self, *args, **kwargs):
    super(CoordinatorForm, self).__init__(*args, **kwargs)
    self.fields['phone'].widget = USPhoneNumberMultiWidget()
  class Meta:
    model = Coordinator
    exclude = ('user','approved', 'tags','assistant', 'coordinator_agreement', 'assistant_agreement', 'area','party_size', )

class CoordinatorProfileForm(forms.ModelForm):
  def __init__(self, *args, **kwargs):
    super(CoordinatorProfileForm, self).__init__(*args, **kwargs)
    for key in self.fields:
      field = self.fields[key]
      if key == 'myself':
        field.widget = ThreeWordsMultiWidget()
      else:
        field.widget = forms.Textarea()
  class Meta:
    model = CoordinatorProfile
    exclude = ('coordinator',)

class CoordinatorGalleryForm(forms.ModelForm):
  class Meta:
    model = CoordinatorGallery
    exclude = ('coordinator',)

class QuizPart1Form(forms.Form):
  arrival = forms.ChoiceField(
    label = "Who do you check in with first when you arrive to the venue?", 
    choices = ( (1, 'Your team'), (2, 'The bride'), (3, 'The venue manager'), (4, 'The photographer'), (5, 'The caterer') ),
    widget = forms.RadioSelect
  )
  
  priority = forms.ChoiceField(widget=forms.RadioSelect,
    label = "Your production is running behind schedule and the ceremony is scheduled to start in 30 minutes. Of the following outstanding items, which is your highest priority?",
    choices = ( 
      (1, 'Placing the escort cards and table numbers.'), 
      (2, 'Getting the caterer situated.'),
      (3, 'Finishing the pre-ceremony refreshment area.'),
      (4, 'Ensuring that the wedding party is in place.'),
      (5, 'Conducting the ceremony sound check.')
    )
  )
  timing = forms.ChoiceField(widget=forms.RadioSelect,
    label = "The dj is suggesting that toast timing be moved between dinner and dancing, instead of during dinner. What do you do?",
    choices = (
      (1, 'Firmly stick to the planned timeline.'), 
      (2, 'Consider the options, and make small adjustments that won\'t affect the flow.'),
      (3, 'Present the options to the couple for the final decision.')
    )
  )
  venue = forms.ChoiceField(widget=forms.RadioSelect,
    label = "It's your first time working at this venue. Which of the following tasks do you expect a venue manager to handle?",
    choices = (
      (1, 'Cleaning up spills and keeping bathrooms tidy.'),
      (2, 'Overseeing security.'),
      (3, 'Setting up decor to go on the venue walls.'),
      (4, 'Accepting deliveries.'),
      (5, 'Welcoming guests to the ceremony area.')
    )
  )
  turnover = forms.ChoiceField(widget=forms.RadioSelect,
    label = "How long should it take to turn over a ceremony space to a dinner reception for 100 guests?",
    choices = (
      (1, '15 minutes.'),
      (2, '45 minutes.'),
      (3, '2 hours.')
    )
  )
  leaving = forms.ChoiceField(widget=forms.RadioSelect,
    label = "It's the end of the night, and your team is ready to go. When is it okay to leave the venue?",
    choices = (
      (1, 'After the floors have been swept.'),
      (2, 'After a full walk-through with the venue manager.'),
      (3, 'After all the other vendors have left.'),
    )
  )
class QuizPart2Form(forms.Form):
  prep = forms.ChoiceField(widget = forms.RadioSelect,
    label = "What's the minimum amount of preparation time that a full-service caterer needs to set-up?", 
    choices = (
      (1, '1 hour'),
      (2, '3 hours'),
      (3, '5 hours'),
    )
  )
  try_food = forms.ChoiceField(widget = forms.RadioSelect,
    label = "Do you try all the food before it goes out?", 
    choices = (
      (1, 'Yes'),
      (2, 'No'),
    )
  )
  meals = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The caterer has let you know that the client did not order vendor meals. What do you do?", 
    choices = (
      (1, 'Wait until all the food has gone out and see if you can sneak away scraps at the end.'),
      (2, 'Gather money together from the vendors and order pizza delivered.'),
      (3, 'Let everyone know that they can take from the buffet.'),
    )
  )
  cake = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The cake arrives 4 hours before the wedding, where does it go?", 
    choices = (
      (1, 'On the cake table'),
      (2, 'In the fridge'),
      (3, 'In the kitchen'),
    )
  )
  table = forms.ChoiceField(widget = forms.RadioSelect,
    label = "Which table would you put a 3 tier cake with a 24\" base on?", 
    choices = (
      (1, '24" round'),
      (2, '36" round (high)'),
      (3, '48" round'),
      (4, '60" round'),
      (5, "6' banquet"),
    )
  )
  flowers = forms.ChoiceField(widget = forms.RadioSelect,
    label = "You noticed that the flowers placed on the cake by the florist are not holding up. The baker and florist are gone. What do you do?", 
    choices = (
      (1, 'Fix the cake'),
      (2, 'Call the florist'),
      (3, 'Call the baker'),
    )
  )
class QuizPart3Form(forms.Form):
  fuse = forms.ChoiceField(widget = forms.RadioSelect,
    label = "Something keeps blowing out the circuits. Which of these is most likely to be the problem?", 
    choices = (
      (1, 'The LED uplight.'),
      (2, 'The video projector.'),
      (3, 'The 100 cup coffee maker.'),
      (4, 'The DJ\'s two speakers.'), 
      (5, 'The iphone charger.'),
    )
  )
  mc = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The couple hasn't specified who will be the MC. Who do you put in charge to do this? ", 
    choices = (
      (1, 'The DJ'),
      (2, 'The father of the bride'),
      (3, 'Handle it yourself'),
    )
  )
  projector = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The projector isn't working. Which cable could you be missing?", 
    choices = (
      (1, 'BNC'),
      (2, 'SVGA'),
      (3, 'XLR'),
    )
  )
  aisle = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The mothers of the bride and groom are arguing about who goes down the aisle first. Who is right?", 
    choices = (
      (1, 'MOG'),
      (2, 'MOB'),
    )
  )
  kids = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The flower girl and ring bearer won't go down the aisle and their parents are seated. What do you do?", 
    choices = (
      (1, 'Try coaxing them down by promising candy'),
      (2, 'Find their parents, let them handle it'),
      (3, 'Leave them alone and give the rings to the maid of honor'),
    )
  )
  groom = forms.ChoiceField(widget = forms.RadioSelect,
    label = "No one knows where the groom is, from whom do you ask for help?", 
    choices = (
      (1, 'The bride'),
      (2, 'The best man'),
      (3, 'The mother of the groom'),
    )
  )
  tables = forms.ChoiceField(widget = forms.RadioSelect,
    label = "It's impossible to see the handmade table numbers that the bride made. What do you do?", 
    choices = (
      (1, 'Make new ones and add it to the table. '),
      (2, 'Have your assistants ready to usher guests to their tables.'),
      (3, 'Do nothing, trust it will all sort itself out.'),
    )
  )
  drunk = forms.ChoiceField(widget = forms.RadioSelect,
    label = "One of the groomsmen is pretty drunk and sloppy. What do you do?", 
    choices = (
      (1, 'Tell another groomsman to keep his boy in check.'),
      (2, 'Keep your distance.'),
      (3, 'Alert the bartender, security, and valet to keep an eye on him.'),
    )
  )
  usher = forms.ChoiceField(widget = forms.RadioSelect,
    label = "How do you usher guests out at the end of the night? ", 
    choices = (
      (1, 'Turn the house lights on.'),
      (2, 'Have the DJ announce last song.'),
      (3, 'Ask security to escort them out.'),
      (4, 'Naturally let guests find their way out.'),
      (5, 'Start cleaning up, they\'ll get the hint.'),
    )
  )


class QuizPart4Form(forms.Form):
  tasks = forms.ChoiceField(widget = forms.RadioSelect,
    label = "Which of these tasks can you hand over to an assistant with zero level experience in coordinating weddings?", 
    choices = (
      (1, 'Wrangling the wedding party.'),
      (2, 'Keeping the caterer informed of how the wedding is unfolding.'),
      (3, 'Putting up the decorations.'),
      (4, 'Keeping track of the couple\'s personal items.'),
      (5, 'All of the above.'),
      (6, 'None of the above.'),
    )
  )
  meeting = forms.ChoiceField(widget = forms.RadioSelect,
    label = "How important is having a team meeting with your assistant?", 
    choices = (
      (1, 'Very important.'),
      (2, 'Kind of important.'),
      (3, 'Not important.'),
    )
  )
  slacker = forms.ChoiceField(widget = forms.RadioSelect,
    label = "You caught your assistant slacking off. What is likely the problem?", 
    choices = (
      (1, 'You haven\'t given her enough to do.'),
      (2, 'She doesn\'t under that she was to check in after completing her last task.'),
      (3, 'She\'s not proactive enough.'),
      (4, 'All of the above.'),
      (5, 'None of the above.'),
    )
  )
  safety = forms.ChoiceField(widget = forms.RadioSelect,
    label = "Which of these do you feel comfortable with having your assistant do?", 
    choices = (
      (1, 'Get up on an 8\' ladder.'),
      (2, 'Light fireworks.'),
      (3, 'Lift things over 25 pounds.'),
      (4, 'All of the above.'),
      (5, 'None of the above.'),
    )
  )
  accident = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The bride's grandfather just fell and cracked his head open. What's the first thing you do?", 
    choices = (
      (1, 'Move him discreetly to a quiet place.'),
      (2, 'Stay with him and call 911'),
      (3, 'Run to find his caretaker.'),
    )
  )
  hanging = forms.ChoiceField(widget = forms.RadioSelect,
    label = "The hanging chandeliers just don't look safe. What do you do?", 
    choices = (
      (1, 'Try to make them safer.'),
      (2, 'Remove them.'),
      (3, 'Cross your fingers.'),
    )
  )

class QuizPart5Form(forms.Form):
  solo = forms.CharField(widget=forms.Textarea, label="How many weddings have you coordinated on your own?", max_length=255)
  assists = forms.CharField(widget=forms.Textarea, label="Have many weddings have you assisted?", max_length=255)
  types = forms.CharField(widget=forms.Textarea, label="What types of wedding ceremonies are you familiar with?", max_length=255)
  describe = forms.CharField(widget=forms.Textarea, label="How do you describe a DIY wedding?", max_length=255)
  managing = forms.CharField(widget=forms.Textarea, label="What previous experience do you have managing a team?", max_length=255)
  venues = forms.CharField(widget=forms.Textarea, label="Which venues have you worked at?", max_length=255)
  why = forms.CharField(widget=forms.Textarea, label="Why do you want this job?", max_length=255)
