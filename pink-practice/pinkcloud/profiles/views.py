from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.formtools.wizard.views import CookieWizardView
from django.views.generic import TemplateView, FormView
from django.contrib.auth.models import User
from registration.backends import  get_backend
from django.core.files.storage import FileSystemStorage
from django.utils import simplejson as json
from django.contrib import messages
from django.db import transaction

from models import *
from forms import *

fs = FileSystemStorage("/tmp/pink/uploads")
def anonymous_required( view_function, redirect_to = None ):
    return AnonymousRequired( view_function, redirect_to )

class AnonymousRequired( object ):
    def __init__( self, view_function, redirect_to ):
        if redirect_to is None:
            from django.conf import settings
            redirect_to = settings.LOGIN_REDIRECT_URL
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__( self, request, *args, **kwargs ):
        if request.user is not None and request.user.is_authenticated():
            return HttpResponseRedirect( self.redirect_to ) 
        return self.view_function( request, *args, **kwargs )

class VendorWizard(CookieWizardView):
  def done(self, form_list, **kwargs):
    user = self.save_user(form_list[0].cleaned_data)
    #coordinator = self.save_coordinator(form_list[1].cleaned_data, user)
  
    return HttpResponseRedirect("/profile/")

class CoordinatorWizard(CookieWizardView):
  file_storage = fs
  def save_user(self, form):
    user = get_backend().register(form['username'], form['email1'], form['first_name'], form['last_name'], 3, self.request)
    profile = Profile(user=user,user_type=3)
    profile.save()
    return user

  def save_coordinator(self, form, user):
    coordinator = Coordinator(user=user, **form)
    coordinator.save()
    return coordinator

  @transaction.commit_manually
  def done(self, form_list, **kwargs):
    user = None
    coordinator = None
    quiz = None
    try:
      user = self.save_user(form_list[0].cleaned_data)
      coordinator = self.save_coordinator(form_list[1].cleaned_data, user)
      all_quiz = {}
      all_quiz.update(form_list[2].cleaned_data)
      all_quiz.update(form_list[3].cleaned_data)
      all_quiz.update(form_list[4].cleaned_data)
      all_quiz.update(form_list[5].cleaned_data)
      all_quiz.update(form_list[6].cleaned_data)
      quiz = CoordinatorQuiz()
      quiz.coordinator = coordinator
      quiz.data = json.dumps(all_quiz)
      quiz.save()
    except Exception ,e:
      transaction.rollback()
      messages.error(self.request, "There was an error saving your registration. We're looking into it, please try again at a later time.")
      return HttpResponseRedirect("/registration/coordinator/")
    else:
      transaction.commit()
      return HttpResponseRedirect("/registration/coordinator/thanks/")
class CoordinatorThanksView(TemplateView):
  template_name = "registration/coordinator_thanks.html"

class ProfileView(TemplateView):
  template_name="profiles/profile.html"

  def get_context_data(self, **kwargs):
    context = super(ProfileView, self).get_context_data(**kwargs)
    proxy = self.request.user.get_profile()
    profile = proxy.get()
    if profile:
      context['user'] = self.request.user
      context['user_type'] = proxy.user_type
      context['profile'] = profile
      context['gallery'] = profile.get_gallery()
      context['public'] = profile.get_public_profile()
      profile_done = False
      gallery_done = False
      if context['public'] is None or context['public'].is_complete() is False:
        messages.error(self.request, "Your Profile is incomplete. <a href=\"/profile/edit/\">Click here to finish!</a>")
      else: profile_done = True
      if context['gallery'] is None or context['gallery'].is_complete() is False:
        messages.error(self.request, "Your Gallery is incomplete. <a href=\"/profile/gallery/\">Click here to finish!</a>")
      else: gallery_done = True
      if gallery_done and profile_done:
        if profile.approved is PROFILE_STATUS_INCOMPLETE:
          messages.success(self.request, "You're almost there! <a href='/profile/submit/'>Click here to submit your profile for review!</a>")
        if profile.approved is PROFILE_STATUS_SUBMITTED:
          messages.success(self.request, "Hang in there, we're reviewing your profile.")
    return context

class ProfileEditView(FormView):
  template_name = "profiles/edit.html"

  def get_profile(self, request):
    return self.request.user.get_profile().get()

  def get_form(self, request):
    profile = self.get_profile(request)
    public = profile.get_public_profile()
    if request.method == "POST":
      return public.get_form()(request.POST, instance=public)
    else:
      return public.get_form()(instance=public)

  def get(self, request, *args, **kwargs):
    form = self.get_form(request)
    return self.render_to_response(self.get_context_data(form=form))

  def post(self, request, *args, **kwargs):
    public = self.get_profile(request).get_public_profile()
    form = self.get_form(request)
    if form.is_valid():
      try:
        form.save()
        messages.success(self.request, "Profile saved!")
      except:
        messages.error(self.request, "There was an error saving your profile. We're looking into it, please try again at a later time.")
    else:
      messages.error(self.request, "Form is invalid.")
    return self.get(request, *args, **kwargs)

class GalleryEditView(FormView):
  template_name = "profiles/gallery_edit.html"

  def get_profile(self, request):
    return self.request.user.get_profile().get()

  def get_form(self, request):
    profile = self.get_profile(request)
    gallery = profile.get_gallery()
    if request.method == "POST":
      return gallery.get_form()(request.POST, request.FILES, instance=gallery)
    else:
      return gallery.get_form()(instance=gallery)

  def get(self, request, *args, **kwargs):
    form = self.get_form(request)
    return self.render_to_response(self.get_context_data(form=form))

  def post(self, request, *args, **kwargs):
    gallery = self.get_profile(request).get_gallery()
    form = self.get_form(request)
    if form.is_valid():
      try:
        form.save()
        messages.success(self.request, "Gallery saved!")
      except Except, e:
        messages.error(self.request, "There was an error saving your gallery. We're looking into it, please try again at a later time.")
      return HttpResponseRedirect("/profile/gallery/")
    else:
      messages.error(self.request, "Form is invalid.")
      return self.render_to_response(self.get_context_data(form=form))

def profile_submit(request):
  profile = request.user.get_profile()
  if profile.user_type == 3:
    return coordinator_profile_submit(request, profile)
  return HttpResponseRedirect("/profile/")

def coordinator_profile_submit(request, proxy):
  profile = proxy.get()
  if profile.is_complete():
    if profile.approved is PROFILE_STATUS_INCOMPLETE:
      profile.approved = PROFILE_STATUS_SUBMITTED
      profile.save()
    else:
      messages.error(self.request, "You've already submitted your profile.")
  else:
    messages.error(self.request, "Your profile is not yet complete.")
  return HttpResponseRedirect("/profile/")

def coordinator_agreement(request):
  pass

def assistant_agreement(request):
  pass
