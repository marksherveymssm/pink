from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from django.views.generic.simple import redirect_to
from views import *


urlpatterns = patterns('',
    url(r'^$', login_required(ProfileView.as_view())),
    url(r'^edit/$', login_required(ProfileEditView.as_view())),
    url(r'^gallery/$', login_required(GalleryEditView.as_view())),
    url(r'^submit/$', login_required(profile_submit)),
)
