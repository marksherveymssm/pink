from django.contrib import admin
from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin
from models import *
from forms import *
from django import forms
from registration.models import RegistrationProfile
from registration.admin import RegistrationAdmin
from django.core import urlresolvers
from django.utils import simplejson as json
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe

import reversion

admin.site.unregister(User)
admin.site.unregister(RegistrationProfile)
def get_user_profile_link(instance):
  try:
    t = instance.get_profile().user_type
  except:
    return None
  if t == 1:
    obj = Customer.objects.get(user=instance)
    url = urlresolvers.reverse("admin:profiles_customer_change", args=(obj.id,))
  elif t == 2:
    obj = Vendor.objects.get(user=instance)
    url = urlresolvers.reverse("admin:profiles_vendor_change", args=(obj.id,))
  elif t == 3:
    obj = Coordinator.objects.get(user=instance)
    url = urlresolvers.reverse("admin:profiles_coordinator_change", args=(obj.id,))
  else:
    return None
  return "<a href={0}>Profile</a>".format(url)

class QuizWidget(forms.Widget):
  def render(self, name, value, attrs=None):
    try:
      value = json.loads(value)
      q1 = QuizPart1Form(initial=value)
      q2 = QuizPart2Form(initial=value)
      q3 = QuizPart3Form(initial=value)
      q4 = QuizPart4Form(initial=value)
      q5 = QuizPart5Form(initial=value)
      output = []
      output.append("<table>")
      output.append("<tr><th colspan='2'>!! THIS DATA IS READ-ONLY !!</th></tr>")
      output.append(str(q1))
      output.append(str(q2))
      output.append(str(q3))
      output.append(str(q4))
      output.append(str(q5))
      output.append("</table>")
      return mark_safe(u''.join(output))
    except: return u''
  def _has_changed(self, initial, data):
    return False

class VendorInline(admin.StackedInline):
  model = Vendor
class CustomerInline(admin.StackedInline):
  model = Customer
class CoordinatorInline(admin.StackedInline):
  model = Coordinator
class ProfileInline(admin.StackedInline):
  model = Profile
class CompanyInline(admin.StackedInline):
  model = CompanyOverview
class ExclusiveInline(admin.StackedInline):
  model = ExclusiveOffer

class CoordinatorProfileInline(admin.StackedInline):
  model = CoordinatorProfile
  def formfield_for_dbfield(self, db_field, **kwargs):
    kwargs['widget'] = forms.Textarea()
    return super(CoordinatorProfileInline, self).formfield_for_dbfield(db_field, **kwargs)

class CoordinatorGalleryInline(admin.StackedInline):
  model = CoordinatorGallery

class QuizInline(admin.StackedInline):
  model = CoordinatorQuiz
  def formfield_for_dbfield(self, db_field, **kwargs):
    if db_field.name == 'data':
      kwargs['widget'] = QuizWidget()
      kwargs['label'] = "Quiz:"
      kwargs['required'] = False
    return super(QuizInline, self).formfield_for_dbfield(db_field, **kwargs)

class RegAdmin(RegistrationAdmin):
  list_display = ('user', 'get_profile', 'get_profile_type', 'get_status_display',)
  search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email',)
  def get_profile(self, instance):
    return get_user_profile_link(instance.user)
  def get_profile_type(self, instance):
    obj =  instance.user.get_profile().get_user_type_display()
    return obj
  get_profile.short_description = "User Profile"
  get_profile.allow_tags = True
  get_profile_type.short_description = "User Type"

admin.site.register(RegistrationProfile, RegAdmin)

class UserProfileAdmin(UserAdmin):
  inlines = [ ProfileInline ]
  list_display = ('username', 'email', 'is_staff', 'get_profile_type', 'get_profile',)
  def get_profile_type(self, instance):
    return instance.get_profile().get_user_type_display()
  def get_profile(self, instance):
    return get_user_profile_link(instance)
  get_profile_type.short_description = "User Type"
  get_profile.short_description = "Profile"
  get_profile.allow_tags = True

admin.site.register(User, UserProfileAdmin)

class CustomerAdmin(admin.ModelAdmin):
  pass

class VendorAdmin(reversion.VersionAdmin):
  inlines = [ CompanyInline, ExclusiveInline ]
  form = make_ajax_form(Vendor, {'user':'user'})
  list_display = ('get_username', 'approved', 'category', 'company_name', 'get_links',)
  list_filter = ('approved','category','state',)
  search_fields = ('user__email', 'user__username', 'company_name', 'contact_name', 'address', 'city', )
  def get_username(self, instance):
    return instance.user.username
  def get_links(self, instance):
    ret = ""
    if instance.website:
      ret += "<a href='{0}' target='_blank'>Website</a>&nbsp;"
    if instance.facebook:
      ret += "<a href='{0}' target='_blank'>Facebook</a>"
    return ret
  get_username.short_description = "Username"
  get_links.short_description = "Links"
  get_links.allow_tags = True

class CoordinatorAdmin(reversion.VersionAdmin):
  inlines = [ CoordinatorProfileInline, CoordinatorGalleryInline, QuizInline ]
  #form = make_ajax_form(Coordinator, {'user':'user'})
  list_display = ('get_username','get_firstname', 'get_lastname', 'approved', 'get_links',)
  list_filter = ('approved',)
  search_fields = ('user__email', 'user__username', 'user__first_name', 'user__last_name', 'address', 'city', )
  readonly_fields = ( 'user', )
  def get_username(self, instance):
    return instance.user.username
  def get_firstname(self, instance):
    return instance.user.first_name
  def get_lastname(self, instance):
    return instance.user.last_name
  def get_links(self, instance):
    ret = ""
    if instance.website:
      ret += "<a href='{0}' target='_blank'>Website</a>&nbsp;"
    if instance.facebook:
      ret += "<a href='{0}' target='_blank'>Facebook</a>"
    return ret
  get_username.short_description = "Username"
  get_firstname.short_description = "First Name"
  get_lastname.short_description = "Last Name"
  get_links.short_description = "Links"
  get_links.allow_tags = True

class CeremonyAdmin(admin.ModelAdmin):
  pass
class ReceptionAdmin(admin.ModelAdmin):
  pass

#admin.site.register(Customer, CustomerAdmin)
#admin.site.register(Vendor, VendorAdmin)
#admin.site.register(Coordinator, CoordinatorAdmin)
