from django.db import models
from registration.signals import *
from django.contrib.localflavor.us.models import *
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from json_field import JSONField
from django.dispatch import receiver
from stdimage import StdImageField

PROFILE_STATUS_INCOMPLETE = 0
PROFILE_STATUS_SUBMITTED = 1
PROFILE_STATUS_APPROVED = 2
PROFILE_STATUS_DENIED = 3 
PROFILE_STATUSES = (
  (PROFILE_STATUS_INCOMPLETE, 'Incomplete'),
  (PROFILE_STATUS_SUBMITTED, 'Submitted'),
  (PROFILE_STATUS_APPROVED, 'Approved'),
  (PROFILE_STATUS_DENIED, 'Denied'),
)
YEARS = [(i, i) for i in range(11)]
YEARS = YEARS + [(11, '11 - 19'), (12, '20+'),]
MINIMUM_ORDER = (
  (1, "$1 - $99"),
  (2, "$100 - $249"),
  (3, "$250 - $499"),
  (4, "$500 - $749"),
  (5, "$750 - $999"),
  (6, "$1000 - $1499"),
  (7, "$1500 - $2999"),
  (8, "$3000 - $4999"),
  (9, "$5000 - $7499"),
  (10, "$7500 - $9999"),
  (11, "$10,000 - $14,999"),
  (12, "$15,000 - $29,000"),
  (13, "$30,000+"),
)
AREAS = (
  (1, "Los Angeles"),
  (2, "Orange County"),
  (3, "Long Beach"),
  (4, "San Diego"),
  (5, "Santa Barbara"),
  (6, "Palm Springs"),
  (7, "San Francisco"),
  (8, "Nationwide"),
)
PARTY_SIZES = (
    (1, ' 100 or less'),
    (2, '100 to 200'),
    (3, '200 or more'),
)
class Profile(models.Model):
  USER_TYPES = (
      (1, "Couple"),
      (2, "Vendor"),
      (3, "Coordinator"),
  )
  user = models.OneToOneField(User)
  user_type = models.PositiveSmallIntegerField(default=1, choices=USER_TYPES)

  def get(self):
    if self.user_type is 3:
      return Coordinator.objects.get(user=self.user)
    elif self.user_type is 2:
      return Vendor.objects.get(user=self.user)
    else:
      return Customer.objects.get(user=self.user)

class Vendor(models.Model):
  VENDOR_CATEGORIES = (
      (1, "Apparel & Accessories"),
      (2, "A/V & Lighting"),
      (3, "Cakes & Desserts"),
      (4, "Catering"),
      (5, "DJs"),
      (6, "Floral Design"),
      (7, "Hair & Makeup"),
      (8, "Health & Wellness"),
      (9, "Invitations"),
      (10, "Musicians"),
      (11, "Officiant"),
      (12, "Photographer"),
      (13, "Rentals"),
      (20, "Transportation"),
      (14, "Videographer"),
      (15, "Miscellaneous"),
      (16, "Fashion & Accessories"),
      (17, "Health & Wellness"),
      (18, "Other"),
  )
  user = models.OneToOneField(User)
  approved = models.PositiveIntegerField(choices=PROFILE_STATUSES, null=False, blank=False, default=PROFILE_STATUS_INCOMPLETE)
  category = models.PositiveIntegerField(choices=VENDOR_CATEGORIES, null=True, blank=False)
  company_name = models.CharField(max_length=255, null=True, blank=False)
  contact_name = models.CharField(max_length=255, null=True, blank=False)
  phone = PhoneNumberField()
  address = models.CharField(max_length=255, null=True, blank=True)
  address2 = models.CharField(max_length=255, null=True, blank=True)
  city = models.CharField(max_length=255, null=True, blank=True)
  state = USStateField()
  zipcode = models.CharField(null=True, blank=True, max_length=7)
  locations_served = models.CommaSeparatedIntegerField(max_length=10, choices=AREAS)
  years_in_business = models.PositiveIntegerField(null=False, blank=False, choices=YEARS)
  minimum_order = models.PositiveIntegerField(null=False, blank=False, choices=MINIMUM_ORDER)
  website = models.URLField(null=True, blank=True)
  facebook = models.URLField(null=True, blank=True)
  pinterest = models.URLField(null=True, blank=True)
  youtube = models.URLField(null=True, blank=True)
  vimeo = models.URLField(null=True, blank=True)
  twitter = models.CharField(null=True, blank=True, max_length=100)
  tags = TaggableManager()
  staff_pick = models.BooleanField(null=False, blank=False, default=False)
  
  def __unicode__(self):
    return self.user.first_name + ' ' + self.user.last_name

class CompanyOverview(models.Model):
  vendor = models.OneToOneField(Vendor)
  mission_statement = models.TextField(blank=False, null=False)
  company_history = models.TextField(blank=False, null=False)
  awards_and_press = models.TextField(blank=False, null=False)
  service_menu = models.TextField(blank=False, null=False)

class ExclusiveOffer(models.Model):
  vendor = models.OneToOneField(Vendor)
  title = models.CharField(max_length=255, null=False, blank=False)
  price = models.DecimalField(max_digits=6, decimal_places=2, null=False, blank=False)
  description = models.TextField(null=False, blank=False)
  restrictions = models.TextField(null=False, blank=False)
  tags = TaggableManager()

class Coordinator(models.Model):
  user = models.OneToOneField(User)
  approved = models.PositiveIntegerField(choices=PROFILE_STATUSES, null=False, blank=False, default=PROFILE_STATUS_INCOMPLETE)
  approved.verbose_name = "Profile Status"
  assistant = models.BooleanField(null=False, blank=False, default=False)
  assistant.verbose_name = "Is an assistant?"
  address = models.CharField(max_length=255, null=False, blank=False)
  address2 = models.CharField(max_length=255, null=True, blank=True)
  city = models.CharField(max_length=255, null=False, blank=False)
  state = USStateField()
  zipcode = models.CharField(null=False, blank=False, max_length=7)
  area = models.PositiveIntegerField(choices=AREAS, null=False, blank=False, default=1)
  phone = PhoneNumberField(null=False, blank=False)
  skype = models.CharField(null=True, blank=True, max_length=100)
  facebook = models.URLField(null=True, blank=True)
  how = models.CharField(null=True, blank=True, max_length=255, verbose_name="How did you hear about PC9?")
  resume = models.FileField(upload_to="resumes/", null=True, blank=True)
  cover_letter = models.FileField(upload_to="resumes/", null=True, blank=True)
  tags = TaggableManager(blank=True)

  coordinator_agreement = models.BooleanField(null=False, blank=False, default=False)
  coordinator_agreement.verbose_name = "Agrees to Coordinator Agreement ( if applicable )"
  assistant_agreement = models.BooleanField(null=False, blank=False, default=False)
  assistant_agreement.verbose_name = "Agrees to Assistant Agreement ( if applicable )"
  party_size = models.PositiveIntegerField(choices=PARTY_SIZES, blank=False, null=False, default=1)

  def get_gallery(self):
    try:
      return CoordinatorGallery.objects.get(coordinator=self)
    except:
      try:
        gallery = CoordinatorGallery(coordinator=self)
        gallery.save()
        return gallery
      except:
        return None

  def get_public_profile(self):
    try:
      return CoordinatorProfile.objects.get(coordinator=self)
    except:
      try:
        profile = CoordinatorProfile(coordinator=self)
        profile.save()
        return profile
      except:
        return None
  def is_complete(self):
    return self.get_gallery().is_complete() and self.get_public_profile().is_complete() 
  
  def __unicode__(self):
    return self.user.first_name + ' ' + self.user.last_name

class CoordinatorQuiz(models.Model):
  coordinator = models.OneToOneField(Coordinator, blank=False, null=False)
  data = JSONField(simple_formfield=True)

class CoordinatorProfile(models.Model):
  coordinator = models.OneToOneField(Coordinator, blank=False, null=False)
  myself = models.CharField(blank=True, null=False, max_length=100)
  myself.verbose_name = "How I'd describe myself in three words"
  moment = models.CharField(blank=True, null=False, max_length=255)
  moment.verbose_name = "My favorite wedding moment"
  memory = models.CharField(blank=True, null=False, max_length=255)
  memory.verbose_name = "My favorite childhood memory"
  some_days = models.CharField(blank=True, null=False, max_length=255)
  some_days.verbose_name = "Some days I secretly want to"
  random = models.CharField(blank=True, null=False, max_length=255)
  random.verbose_name = "Random thought"
  obsessed = models.CharField(blank=True, null=False, max_length=255)
  obsessed.verbose_name = "I am currently obsessed with"
  because = models.CharField(blank=True, null=False, max_length=255)
  because.verbose_name = "I'm a wedding coordinator because"
  good = models.CharField(blank=True, null=False, max_length=255)
  good.verbose_name = "I am majorly good at"
  refining = models.CharField(blank=False, null=False, max_length=255)
  refining.verbose_name = "Currently, I'm focusing on refining this skiil"
  couples = models.CharField(blank=True, null=False, max_length=255)
  couples.verbose_name = "Three adjectives to describe my favorite couples to work with"
  working = models.CharField(blank=True, null=False, max_length=255)
  working.verbose_name = "The best part about working with Pink Cloud 9 is"
  advice = models.CharField(blank=True, null=False, max_length=255)
  advice.verbose_name = "My advice for planning your own wedding"

  def is_complete(self):
    for field in self._meta.fields:
      if field.name in ['coordinator', 'id']: continue
      val = getattr(self, field.name)
      if val is None or val is "":
        return False
    return True

  def get_form(self):
    from forms import CoordinatorProfileForm
    return CoordinatorProfileForm

class CoordinatorGallery(models.Model):
  coordinator = models.OneToOneField(Coordinator, blank=False, null=False)
  portrait = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  portrait.verbose_name = "Portrait / Headshot"
  car = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  car.verbose_name = "Your and your car"
  tools = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  tools.verbose_name = "Essential Emergency Tools"
  inspiration = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  inspiration.verbose_name = "An Inspiration Board"
  timeline = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  timeline.verbose_name = "A past timeline with your notes"
  meeting = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  meeting.verbose_name = "Leading a team meeting"
  floorplan = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  floorplan.verbose_name = "Floorplan (Hand Drawn or Created)"
  setting = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  setting.verbose_name = "Table setting design element"
  couple = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  couple.verbose_name = "One of your past couples"
  thanks = StdImageField(upload_to='users/', size=(480, 240), thumbnail_size=(100, 75), blank=True, null=False)
  thanks.verbose_name = "Thank you notes"

  def is_complete(self):
    for field in self._meta.fields:
      if field.name in ['coordinator', 'id']: continue
      val = getattr(self, field.name)
      if val.name is u'':
        return False
    return True

  def get_form(self):
    from forms import CoordinatorGalleryForm
    return CoordinatorGalleryForm

class Ceremony(models.Model):
  user = models.OneToOneField(User)
  CEREMONY_TYPES = (
      (1, 'Christian'),
      (2, 'Jewish'),
      (3, 'Muslim'),
      (4, 'Buddhist'),
      (5, 'Other - Please Describe'),
  )
  ceremony_type = models.PositiveSmallIntegerField(choices=CEREMONY_TYPES, null=True, blank=False)
  type_description = models.TextField(null=True, blank=True)
  location_venue = models.OneToOneField(Vendor, null=True, blank=True)
  location_name = models.CharField(max_length=255, null=True, blank=True)
  location_address = models.CharField(max_length=255, null=True, blank=True)
  location_address2 = models.CharField(max_length=255, null=True, blank=True)
  location_city = models.CharField(max_length=255, null=True, blank=True)
  location_state = USStateField()
  location_zipcode = models.CharField(null=True, blank=True, max_length=7)

  class Meta:
    verbose_name_plural = 'Ceremonies'

class Reception(models.Model):
  user = models.OneToOneField(User)
  location_venue = models.OneToOneField(Vendor, null=True, blank=True)
  location_name = models.CharField(max_length=255, null=True, blank=True)
  location_address = models.CharField(max_length=255, null=True, blank=True)
  location_address2 = models.CharField(max_length=255, null=True, blank=True)
  location_city = models.CharField(max_length=255, null=True, blank=True)
  location_state = USStateField()
  location_zipcode = models.CharField(null=True, blank=True, max_length=5)

class Customer(models.Model):
  user = models.OneToOneField(User)
  bride_name = models.CharField(max_length=255, null=True, blank=False)
  groom_name = models.CharField(max_length=255, null=True, blank=False)
  wedding_date = models.DateField(null=True, blank=False)
  ceremony = models.OneToOneField(Ceremony)
  reception = models.OneToOneField(Reception)
  def __unicode__(self):
    return self.user.first_name + ' ' + self.user.last_name
