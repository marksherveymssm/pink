from django import template

register = template.Library()

@register.simple_tag
def get_verbose_name(obj, attribute):
  return obj._meta.get_field_by_name(attribute)[0].verbose_name
