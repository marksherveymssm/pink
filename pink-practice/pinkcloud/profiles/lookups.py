from ajax_select import LookupChannel
from django.utils.html import escape
from django.db.models import Q
from models import *
from django.contrib.auth.models import User

class UserLookup(LookupChannel):
  model = User
  def get_query(self,q,request):
    return User.objects.filter(Q(username__icontains=q) | Q(email__istartswith=q) | Q(first_name__istartswith=q) | Q(last_name__istartswith=q)).order_by('username')
