Dear {{user.first_name}}, 
{% if user_type == 3 %}
Thank you for taking the time to fill out our application to be a Big Day Coordinator with Pink Cloud 9, we hope you had fun!
Please allow us some time to review your answers, we will be in touch with you within 2 business days. Thank you!

Best Wishes,
The PC9 Team
{% endif %}
